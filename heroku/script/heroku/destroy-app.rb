#!/usr/bin/env ruby
# frozen_string_literal: true

require 'platform-api'

heroku = PlatformAPI.connect_oauth ENV['HEROKU_API_KEY'],
                                   team: ENV['HEROKU_TEAM']

begin
  heroku.app.delete ENV['HEROKU_APP']
  puts 'app removed'
rescue Excon::Error::NotFound # the app doesn't exist
  puts 'not found'
end
