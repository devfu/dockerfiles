#!/usr/bin/env ruby
# frozen_string_literal: true

require 'platform-api'

def app_name
  @app_name ||= ENV['HEROKU_APP']
end

def app_exists?
  !!heroku.app.info(app_name)
rescue Excon::Error::NotFound # the app doesn't exist
  puts 'not found'
  false
end

def find_pipeline
  @find_pipeline ||= heroku.pipeline.info ENV['HEROKU_PIPELINE']
end

def heroku
  @heroku ||= PlatformAPI.connect_oauth ENV['HEROKU_API_KEY'],
                                        team: ENV['HEROKU_TEAM']
end

def hostname
  # strip wildcard domains of their wildcards
  @hostname ||= ENV['DEPLOY_URL'].gsub /^\*./, ''
end

unless app_exists?
  # create app
  heroku.team_app.create name: app_name, team: ENV['HEROKU_TEAM']

  # add on addons on
  heroku.addon.create app_name, plan: 'sendgrid:starter'

  # enable labs features
  heroku.app_feature.update app_name, 'runtime-dyno-metadata', enabled: true

  # configure variables
  heroku.config_var.update app_name,
                           'HOST'           => hostname,
                           'RAILS_ENV'      => ENV['DEPLOY_ENV'],
                           'RELEASE_BRANCH' => ENV['CI_COMMIT_REF_NAME']

  # line up pipes
  heroku.pipeline_coupling.create app:      app_name,
                                  pipeline: find_pipeline['id'],
                                  stage:    ENV['DEPLOY_ENV']

  # add custom domain
  heroku.domain.create app_name, hostname: ENV['DEPLOY_URL']
  heroku.domain.create app_name, hostname: hostname
end
