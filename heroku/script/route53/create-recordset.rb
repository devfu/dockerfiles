#!/usr/bin/env ruby
# frozen_string_literal: true

require 'aws-sdk-route53'

# configure AWS

# Expects the following environment variables to be configured properly
# ENV['AWS_ACCESS_KEY_ID']
# ENV['AWS_REGION']
# ENV['AWS_SECRET_ACCESS_KEY']

aws = Aws::Route53::Resource.new
r53 = aws.client # rubocop:disable Naming/VariableNumber

hostname = "#{ ENV['CI_ENVIRONMENT_SLUG'] }.#{ ENV['DEPLOY_TLD'] }"
target   = "#{ hostname }.herokudns.com"
routes   = { hostname => target }

if ENV['DEPLOY_URL'].match /^\*/
  # this is a wildcard domain, we need to support both
  routes[ENV['DEPLOY_URL']] = "wildcard.#{ target }"
end

changes = routes.map do |name, value|
  {
    action:              'UPSERT',
    resource_record_set: {
      name:             name,
      resource_records: [ { value: value } ],
      ttl:              300,
      type:             'CNAME'
    }
  }
end

record_set_attrs = {
  hosted_zone_id: ENV['AWS_HOSTED_ZONE_ID'],
  change_batch:   {
    comment: "Create #{ ENV['CI_ENVIRONMENT_SLUG'] } review app dns",
    changes: changes
  }
}

response = r53.change_resource_record_sets record_set_attrs
r53.wait_until :resource_record_sets_changed, id: response.change_info.id
